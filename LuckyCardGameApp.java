import java.util.Scanner;

public class LuckyCardGameApp {
    public static void main(String[] args) {
        /* Deck testDeck = new Deck();
        testDeck.shuffle();

        Scanner input = new Scanner(System.in);
        System.out.print("Welcome! How many cards do you wish to remove from the deck? ");
        int cardsRemove = input.nextInt();

        System.out.println(testDeck.length());

        for (int i = 0; i < cardsRemove; i++) {
            testDeck.drawTopCard();
        }
        
        System.out.println("Shuffled deck:");
        testDeck.shuffle();
        System.out.println(testDeck);

        input.close(); */


        GameManager manager = new GameManager();
        int totalPoints = 0;
        System.out.println("Hello! Welcome!");

        while (manager.getNumberOfCards() > 1 && totalPoints < 5) {
            System.out.println(manager);
            totalPoints += manager.calculatePoints(); 
            System.out.println("Total Points: " + totalPoints);
            manager.dealCards();
        }

        System.out.println("****************************************");

        if (totalPoints >= 5) 
            System.out.println("Player wins with " + "\u001B[32m" + totalPoints + "\u001B[0m" + " points");
        else
            System.out.println("Player wins with " + "\u001B[31m" + totalPoints + "\u001B[0m" + " points");


    }
}
