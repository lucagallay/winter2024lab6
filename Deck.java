
import java.util.Random;

public class Deck {
    private int numOfCards;
    private Card[] stack;
    private Random rng;

    public void shuffle() {
        for (int i = numOfCards - 1; i > 0; i--) {
            int randIndex = rng.nextInt(i + 1);
            Card temp = stack[i];
            stack[i] = stack[randIndex];
            stack[randIndex] = temp;
        }
    }

    public int length() {
        return numOfCards;
    }

    public Card drawTopCard() {
        Card topCard = stack[numOfCards - 1];
        Card[] newStack = new Card[numOfCards - 1];
        for (int i = 0; i < numOfCards - 1; i++) {
            newStack[i] = stack[i];
        }

        stack = newStack;
        numOfCards--;
    
        return topCard;
    }
    
    public Deck() {
        rng = new Random();
        stack = new Card[52];
        String[] suits = {"Clubs", "Spades", "Diamonds", "Hearts"};
        int[] ranks = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};

        int i = 0;
        for (int r : ranks) {
            for (String s : suits) {
                stack[i] = new Card(r, s);
                i++;
            }
        }

        numOfCards = stack.length;
    }

    public String toString() {
        String result = "";
        for (Card c : stack) {
            result += c.toString() + "\n";
        }
        return result;
    }

    
}
